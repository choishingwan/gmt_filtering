#include <Eigen/Dense>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

inline std::vector<std::string> split(const std::string& seq,
                                      const std::string& separators = "\t ")
{
    std::size_t prev = 0, pos;
    std::vector<std::string> result;
    while ((pos = seq.find_first_of(separators, prev)) != std::string::npos)
    {
        if (pos > prev) result.emplace_back(seq.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < seq.length()) result.emplace_back(seq.substr(prev, pos - prev));
    return result;
}
inline void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return std::isgraph(ch); }));
}
inline void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return std::isgraph(ch); })
                .base(),
            s.end());
}
inline void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
}
inline std::string ltrimmed(std::string s)
{
    ltrim(s);
    return s;
}
inline std::string rtrimmed(std::string s)
{
    rtrim(s);
    return s;
}
inline std::string trimmed(std::string s)
{
    trim(s);
    return s;
}

inline bool logically_equal(double a, double b, double error_factor = 1.0)
{
    return ((a == b)
            || (std::abs(a - b) < std::abs(std::min(a, b))
                                      * std::numeric_limits<double>::epsilon()
                                      * error_factor));
}
int main(int argc, char* argv[])
{
    if (argc != 3)
    {
        std::cerr << "Error: Did not provide all required parameters"
                  << std::endl;
        std::cerr << "Usage: GMT_Filter <Annot file> <Output name>"
                  << std::endl;
        return -1;
    }
    std::string in_name = argv[1];
    std::ifstream input(in_name.c_str());
    if (!input.is_open())
    {
        std::cerr << "Error: Cannot open file: " << argv[1] << std::endl;
        return -1;
    }
    std::ofstream result_file(argv[2]);
    if (!result_file.is_open())
    {
        std::cerr << "Error: Cannot open file: " << argv[2] << std::endl;
        return -1;
    }
    // must have header
    std::string header;
    std::getline(input, header);
    trim(header);
    auto header_tokens = split(header);
    // we don't really care about the SNP due to structure of annot
    // can always ignore first 4 column as they are CHR BP SNP and CM
    // check if it is then Control, which we will also remove
    if (header_tokens.size() < 6
        || (header_tokens.size() < 7 && header_tokens[4] == "Control"))
    {
        std::cerr << "Error: Not enough gene set for distance calculation"
                  << std::endl;
        return -1;
    }
    const std::size_t start_column_idx = 4 + (header_tokens[4] == "Control");
    const std::size_t num_gene_sets = header_tokens.size() - start_column_idx;
    Eigen::MatrixXd distance =
        Eigen::MatrixXd::Zero(num_gene_sets, num_gene_sets);
    // now transverse the annot file
    std::string line;
    std::vector<double> num_snps_in_set(num_gene_sets, 0.0);
    std::size_t line_read = 0;
    while (std::getline(input, line))
    {
        trim(line);
        if (line.empty()) continue;
        auto&& token = split(line);
        std::vector<std::size_t> has_snp;
        std::cerr << "\rLine: " << line_read++;
        for (std::size_t i = start_column_idx; i < token.size(); ++i)
        {
            if (token[i] == "1")
            {
                auto cur_idx = i - start_column_idx;
                num_snps_in_set[cur_idx] += 1.0;

                for (auto&& idx : has_snp)
                {
                    distance(idx, cur_idx) += 1.0;
                    distance(cur_idx, idx) += 1.0;
                }
                has_snp.push_back(cur_idx);
            }
        }
    }
    std::cerr << std::endl
              << "Finished reading file, now start filtering" << std::endl;
    input.close();
    // now update the matrix to include ratio
    Eigen::VectorXd set_size = Eigen::Map<Eigen::VectorXd>(
        num_snps_in_set.data(),
        static_cast<Eigen::Index>(num_snps_in_set.size()));
    Eigen::MatrixXd portion = distance.array().colwise() / set_size.array();
    // now go through matrix and remove sets with lots of overlap
    Eigen::MatrixXd::Index maxRow;
    auto value = portion.rowwise().sum().maxCoeff(&maxRow);
    while (!logically_equal(value, 0.0))
    {
        result_file << header_tokens[maxRow + start_column_idx] << "\t" << value
                    << std::endl;
        portion.row(maxRow).setZero();
        portion.col(maxRow).setZero();
        value = portion.rowwise().sum().maxCoeff(&maxRow);
    }
    result_file.close();
    return 0;
}
